CVE-2005-3660: Fix unknown
CVE-2007-3719: Fix unknown
CVE-2008-2544: Fix unknown
CVE-2008-4609: Fix unknown
CVE-2010-4563: Fix unknown
CVE-2010-5321: Fix unknown
CVE-2011-4916: Fix unknown
CVE-2011-4917: Fix unknown
CVE-2012-4542: Fix unknown
CVE-2013-7445: Fix unknown
CVE-2015-2877: Fix unknown
CVE-2016-8660: Fix unknown
CVE-2017-13693: Fix unknown
CVE-2017-13694: Fix unknown
CVE-2018-1121: Fix unknown
CVE-2018-12928: Fix unknown
CVE-2018-12929: Fix unknown
CVE-2018-12930: Fix unknown
CVE-2018-12931: Fix unknown
CVE-2018-17977: Fix unknown
CVE-2019-12456: Fix unknown
CVE-2019-15239: Fix not seen in stream
CVE-2019-15290: Fix unknown
CVE-2019-15902: Fix not seen in stream
CVE-2019-16089: Fix unknown
CVE-2019-19378: Fix unknown
CVE-2019-19814: Fix unknown
CVE-2019-20794: Fix unknown
CVE-2020-0347: Fix unknown
CVE-2020-10708: Fix unknown
CVE-2020-11725: Fix unknown
CVE-2020-14304: Fix unknown
CVE-2020-15802: Fix unknown
CVE-2020-24502: Fix unknown
CVE-2020-24503: Fix unknown
CVE-2020-25220: Fix not seen in stream
CVE-2020-26140: Fix unknown
CVE-2020-26142: Fix unknown
CVE-2020-26143: Fix unknown
CVE-2020-26556: Fix unknown
CVE-2020-26557: Fix unknown
CVE-2020-26559: Fix unknown
CVE-2020-26560: Fix unknown
CVE-2020-35501: Fix unknown
CVE-2020-36516: Fix unknown
CVE-2021-0399: Fix unknown
CVE-2021-0695: Fix unknown
CVE-2021-26934: Fix unknown
CVE-2021-33061: Fix not seen in stream
CVE-2021-33655: Fix not seen in stream
CVE-2021-3542: Fix unknown
CVE-2021-3714: Fix unknown
CVE-2021-3847: Fix unknown
CVE-2021-3864: Fix unknown
CVE-2021-3892: Fix unknown
CVE-2021-39800: Fix unknown
CVE-2021-39801: Fix unknown
CVE-2021-39802: Fix unknown
CVE-2022-0168: Fixed with 5.17.2
CVE-2022-0171: Fix not seen in stream
CVE-2022-0400: Fix unknown
CVE-2022-1012: Fixed with 5.17.9
CVE-2022-1015: Fixed with 5.17.1
CVE-2022-1016: Fixed with 5.17.1
CVE-2022-1048: Fixed with 5.17.1
CVE-2022-1116: Fix unknown
CVE-2022-1158: Fixed with 5.17.2
CVE-2022-1184: Fixed with 5.17.14
CVE-2022-1204: Fixed with 5.17.2
CVE-2022-1205: Fixed with 5.17.2
CVE-2022-1247: Fix unknown
CVE-2022-1263: Fixed with 5.17.3
CVE-2022-1353: Fixed with 5.17
CVE-2022-1462: Fix not seen in stream
CVE-2022-1516: Fixed with 5.17.2
CVE-2022-1651: Fixed with 5.17.2
CVE-2022-1652: Fixed with 5.17.10
CVE-2022-1671: Fixed with 5.17.2
CVE-2022-1679: Fix not seen in stream
CVE-2022-1729: Fixed with 5.17.10
CVE-2022-1734: Fixed with 5.17.7
CVE-2022-1789: Fixed with 5.17.12
CVE-2022-1836: Fixed with 5.17.6
CVE-2022-1852: Fixed with 5.17.13
CVE-2022-1882: Fix not seen in stream
CVE-2022-1943: Fixed with 5.17.8
CVE-2022-1966: Fixed with 5.17.13
CVE-2022-1972: Fixed with 5.17.13
CVE-2022-1973: Fixed with 5.17.14
CVE-2022-1974: Fixed with 5.17.7
CVE-2022-1975: Fixed with 5.17.7
CVE-2022-20158: Fixed with 5.17
CVE-2022-20368: Fixed with 5.17
CVE-2022-20369: Fixed with 5.17.2
CVE-2022-2078: Fixed with 5.17.13
CVE-2022-21123: Fix not seen in stream
CVE-2022-21125: Fix not seen in stream
CVE-2022-21166: Fix not seen in stream
CVE-2022-21499: Fixed with 5.17.10
CVE-2022-21505: Fix not seen in stream
CVE-2022-2153: Fixed with 5.17.2
CVE-2022-2209: Fix unknown
CVE-2022-2308: Fix unknown
CVE-2022-2318: Fix not seen in stream
CVE-2022-2380: Fixed with 5.17.2
CVE-2022-23816: Fix not seen in stream
CVE-2022-23825: Fix unknown
CVE-2022-2503: Fixed with 5.17.13
CVE-2022-25265: Fix unknown
CVE-2022-2585: Fix not seen in stream
CVE-2022-2586: Fix not seen in stream
CVE-2022-2588: Fix not seen in stream
CVE-2022-2590: Fix not seen in stream
CVE-2022-26365: Fix not seen in stream
CVE-2022-26373: Fix not seen in stream
CVE-2022-2639: Fixed with 5.17.5
CVE-2022-2663: Fix unknown
CVE-2022-26878: Fix unknown
CVE-2022-28356: Fixed with 5.17.1
CVE-2022-28388: Fixed with 5.17.2
CVE-2022-28389: Fixed with 5.17.2
CVE-2022-28390: Fixed with 5.17.2
CVE-2022-2873: Fixed with 5.17.13
CVE-2022-28796: Fixed with 5.17.1
CVE-2022-28893: Fixed with 5.17.3
CVE-2022-2905: Fix not seen in stream
CVE-2022-29581: Fixed with 5.17.5
CVE-2022-29582: Fixed with 5.17.3
CVE-2022-2959: Fixed with 5.17.13
CVE-2022-2961: Fix unknown
CVE-2022-2977: Fixed with 5.17.1
CVE-2022-2978: Fix unknown
CVE-2022-29900: Fix not seen in stream
CVE-2022-29901: Fix not seen in stream
CVE-2022-29968: Fixed with 5.17.6
CVE-2022-3028: Fix not seen in stream
CVE-2022-30594: Fixed with 5.17.2
CVE-2022-3061: Fix not seen in stream
CVE-2022-3077: Fixed with 5.17.13
CVE-2022-3078: Fixed with 5.17.2
CVE-2022-3169: Fix unknown
CVE-2022-3176: Fix unknown
CVE-2022-3202: Fixed with 5.17.3
CVE-2022-32250: Fixed with 5.17.13
CVE-2022-32296: Fixed with 5.17.9
CVE-2022-32981: Fixed with 5.17.15
CVE-2022-33740: Fix not seen in stream
CVE-2022-33741: Fix not seen in stream
CVE-2022-33742: Fix not seen in stream
CVE-2022-33743: Fix not seen in stream
CVE-2022-33744: Fix not seen in stream
CVE-2022-33981: Fixed with 5.17.6
CVE-2022-34494: Fixed with 5.17.15
CVE-2022-34495: Fixed with 5.17.15
CVE-2022-34918: Fix not seen in stream
CVE-2022-36123: Fix not seen in stream
CVE-2022-36280: Fix unknown
CVE-2022-36402: Fix unknown
CVE-2022-36879: Fix not seen in stream
CVE-2022-36946: Fix not seen in stream
CVE-2022-38096: Fix unknown
CVE-2022-38457: Fix unknown
CVE-2022-39188: Fix not seen in stream
CVE-2022-39189: Fix not seen in stream
CVE-2022-39190: Fix not seen in stream
CVE-2022-39842: Fix not seen in stream
CVE-2022-40133: Fix unknown
CVE-2022-40307: Fix not seen in stream
CVE-2022-40768: Fix unknown
