
# **Linux Kernel CVE Changes**

## Last Update - 19Sep22 22:35

### **New CVEs Added:**

[CVE-2022-2977](cves/CVE-2022-2977)  
[CVE-2022-3176](cves/CVE-2022-3176)  
[CVE-2022-3202](cves/CVE-2022-3202)  
[CVE-2022-36280](cves/CVE-2022-36280)  
[CVE-2022-36402](cves/CVE-2022-36402)  
[CVE-2022-38096](cves/CVE-2022-38096)  
[CVE-2022-38457](cves/CVE-2022-38457)  
[CVE-2022-40133](cves/CVE-2022-40133)  
[CVE-2022-40476](cves/CVE-2022-40476)  
[CVE-2022-40768](cves/CVE-2022-40768)  


### **New Versions Checked:**

[4.14.293](streams/4.14)  
[4.19.258](streams/4.19)  
[4.9.328](streams/4.9)  
[5.10.143](streams/5.10)  
[5.15.68](streams/5.15)  
[5.19.9](streams/5.19)  
[5.4.213](streams/5.4)  


### **Updated CVEs:**

[CVE-2021-4159](cves/CVE-2021-4159)  
[CVE-2022-40307](cves/CVE-2022-40307)  
